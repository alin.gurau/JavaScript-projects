function colorButton() {
  var R = Math.floor(Math.random() * 225);
  var G = Math.floor(Math.random() * 225);
  var B = Math.floor(Math.random() * 225);
  this.event.target.style.background = "RGB(" + R + "," + G + "," + B + ")";
}

function resetButtonsColor() {
  var allButtons = document.getElementsByTagName("button");
  for (var i = 0; i < allButtons.length; i++) {
    allButtons[i].style.background = "#D3D3D3";
  }
}

function oddBlueAndEvenRed() {
  var allButtons = document.getElementsByTagName("button");
  for (i = 0; i <= 100; i++) {
    if (i % 2 == 0) {
      allButtons[i].style.background = "blue";
    } else {
      allButtons[i].style.background = "red";
    }
  }
}

function colorDiagonals() {
  var allButtons = document.getElementsByTagName("button");
  var i = 0,
    j = 9;
  while (i <= 100 && j < 92) {
    allButtons[i].style.background = "yellow";
    allButtons[j].style.background = "yellow";
    i += 11;
    j += 9;
  }
}

function chessBoard() {
  var allButtons = document.getElementsByTagName("button");
  for (var i = 0; i < allButtons.length - 4; i++) {
    if (allButtons[i].id == "chessColors") {
      allButtons[i].style.background = "#FFFFFF";
    } else {
      allButtons[i].style.background = "#000000";
    }
  }
}
